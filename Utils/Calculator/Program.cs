﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the operation by writing two variables and an operation code.");
            Console.WriteLine(
                "Operation codes:" + Environment.NewLine +
                "1 - addition" + Environment.NewLine +
                "2 - subtraction" + Environment.NewLine +
                "3 - division" + Environment.NewLine +
                "4 - multiplication" + Environment.NewLine);
            Console.WriteLine("For example, to divide 4 and 2 write \"4 2 4\".");
            Console.Write("Type here: ");
            string[] read = Console.ReadLine().Split(' ');
            //In case of FormatExceprion
            try
            {
                int a = int.Parse(read[0]);
                int b = int.Parse(read[1]);
                int c = int.Parse(read[2]);
                bool success = Operate(a, b, c);
            }
            catch
            {
                Console.WriteLine("Wrong input!");
            }
            Console.ReadKey();
        }

        static bool Operate(int a, int b, int operation)
        {
            switch (operation)
            {
                //addition
                case 1:
                    Console.WriteLine("The answer is " + (a + b) + ".");
                    return true;
                //subtraction
                case 2:
                    Console.WriteLine("The answer is " + (a - b) + ".");
                    return true;
                //multiplication
                case 3:
                    Console.WriteLine("The answer is " + (a * b) + ".");
                    return true;
                //division
                case 4:
                    if (b == 0)
                    {
                        Console.WriteLine("Divide by Zero!");
                        return false;
                    }
                    Console.WriteLine("The answer is " + (float)(a)/(float)(b) + ".");
                    return true;
                //wrong input
                default:
                    Console.WriteLine("Wrong input!");
                    return false;
            }
        }
    }
}
